<div class="container">
      <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
      <form class="text-center border border-light p-5" method="POST">
        
        <h3 class="mb-5">Sistema Corporativo</h3>
        
       
        <div class="form-outline mb-4">
            <input type="email" id="email" name="email" class="form-control" />
            <label class="form-label" for="email">E-mail</label>
        </div>

        
        <div class="form-outline mb-4">
            <input type="password" id="senha" name= "senha" class="form-control" />
            <label class="form-label" for="senha">Senha</label>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Entrar</button>
        </form>

</div>
</div>